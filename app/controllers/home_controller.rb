# frozen_string_literal: true
#
class HomeController < ApplicationController
  http_basic_authenticate_with name: ENV['WHATSAPP_NAME'], password: ENV['WHATSAPP_PASS'], except: :index

  def index
    @expedient = Expedient.new
    @institutions = Institution.visible.includes(:branches)
    @states = State.includes(:cities)
  end

  def new_whatsapp
    @today = Whatsapp.where(created_at: Time.current.beginning_of_day .. Time.current.end_of_day).order(:created_at)
    @whatsapp = Whatsapp.new
  end

  def register_whatsapp
    @whatsapp = Whatsapp.new params_whatsapp
    if @whatsapp.save
      redirect_to new_whatsapp_url and return
    else
      @today = Whatsapp.where(created_at: Time.current.beginning_of_day .. Time.current.end_of_day).order(:created_at)
      render :new_whatsapp
    end
  end

  private
    def params_whatsapp
      params.require(:whatsapp).permit(:phone, :message, :kind)
    end
end
