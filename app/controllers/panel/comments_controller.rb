# frozen_string_literal: true
module Panel
  #
  class CommentsController < PanelController
    include Panel::Crud
    include Panel::Multiple

    def model
      Comment
    end

    def permits
      [:comment]
    end

    def exportable_fields
      []
    end

    def create
      management = Management.find params[:management_id]
      item = management.expedient
      comment = management.comments.new item_params
      comment.user_id = current_user.id
      success = comment.save
      respond_to do |format|
        # Save assets
        if params[:file]
          params[:file].each do |k, file|
            Asset.create attachment: file, assetable: comment
          end
        end
        format.html {
          return redirect_to panel_expedient_url(item, anchor: "management-#{management.id}")
        }
        format.json {
          render json: {code: 200, item: comment, redirect_url: panel_expedient_url(item, anchor: "management-#{management.id}")}
        }
      end
    end

    def item_params
      perms = params.require(:comment).permit(permits)
      perms
    end


  end
end
