# frozen_string_literal: true
module Panel
  #
  class ManagementsController < PanelController
    include Panel::Crud
    include Panel::Multiple

    def model
      Management
    end

    def permits
      [:kind, :comment, :management_category_id, :deadline_at, :assigned_ids => []]
    end

    def create
      @item = Expedient.find params[:expedient_id]
      @management = Management.new(item_params)
      @management.expedient_id = @item.id
      @management.user_id = current_user.id
      @management.state = 'process'
      success = @management.save
      if success
        @management.events.create(state: 'process', user_id: current_user.id, start_at: Time.current, justification: 'Inicio del proceso')
        # Save assets
        if params[:file]
          params[:file].each do |k, file|
            Asset.create attachment: file, assetable: @management
          end
        end
        respond_to do |format|
          format.html {
            return redirect_to(panel_expedient_url(@item), flash: {saved: true}) # TODO anchor to new management
          }
          format.json {
            render json: {code: 200, item: @event, redirect_url: panel_expedient_url(@item, anchor: "management-#{@management.id}")}  # TODO anchor to new management
          }
        end
      else
        @event = @item.events.new
        @event.start_at = Time.current
        respond_to do |format|
          format.html {
            @errors = @management.errors
            render 'panel/expedients/show'
          }
          format.json {
            render json: {code: 422, errors: @event.errors}
          }
        end
      end

    end

    def update
      success = @item.update_attributes(item_params)
      if success
        respond_to do |format|
          format.html {
            return redirect_to(panel_expedient_url(@item.expedient, anchor: "management-#{@management.id}"), flash: {saved: true})
          }
          format.json {
            render json: {code: 200, item: @event, redirect_url: panel_expedient_url(@item.expedient, anchor: "management-#{@management.id}")}
          }
        end
      else
        respond_to do |format|
          format.html {
            @errors = @management.errors
            render 'panel/expedients/show'
          }
          format.json {
            render json: {code: 422, errors: @event.errors}
          }
        end
      end
    end

    def close
      @management = Management.find params[:id]
      @event = @management.events.new(event_params)
      @event.state = 'closed'
      @event.user_id = current_user.id
      success = @event.save
      if success
        @management.state  = @event.state
        @management.weight = @event.weight
        @management.save
        @management.comments.create(user_id: current_user.id, comment: @event.justification)
      end
      return redirect_to(panel_expedient_url(@management.expedient, anchor: "management-#{@management.id}"))
    end

    def item_params
      perms = params.require(:management).permit(permits)
      perms
    end

    def event_params
      perms = params.require(:event).permit(:start_at, :justification, :weight)
      perms
    end


  end
end
