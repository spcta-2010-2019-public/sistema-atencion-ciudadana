# frozen_string_literal: true
module Panel
  #
  class OccupationsController < PanelController
    include Panel::Crud
    include Panel::Multiple
    
    def model
      Occupation
    end

    def permits
      [:name]
    end

    def exportable_fields
      [:name]
    end

  end
end
