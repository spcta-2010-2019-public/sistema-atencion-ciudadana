# frozen_string_literal: true
module Panel
  #
  class CalendarEventsController < PanelController
    include Panel::Crud
    include Panel::Multiple

    def model
      CalendarEvent
    end

    def create
      @item = model.new item_params
      @item.user_id = current_user.id
      @item.institution_calendar_id = params[:institution_calendar_id]
      return redirect_to(edit_url, flash: { saved: true }) if @item.save

      init_form
      breadcrumbs_for(:new)
      render template: 'concerns/panel/form'
    end

    def permits
      [:name, :description, :kind, :start_at, :end_at, :repetitive]
    end

    def exportable_fields
      [:name]
    end

    def respond_to_html
      add_breadcrumb t('home'), panel_root_url
      add_breadcrumb humanized_model_name(:many), index_url
      render template: 'panel/calendar_events/index'
    end

    def breadcrumbs_for(action)
      add_breadcrumb t('home'), panel_root_url
      add_breadcrumb "Listado de eventos", panel_institution_calendar_url(params[:institution_calendar_id])
      add_breadcrumb t(action.to_s), nil
    end

  end
end
