class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session

  # Overwriting the sign_out redirect path method
  def after_sign_out_path_for(resource_or_scope)
    return panel_root_url if resource_or_scope == :admin
    super
  end

  def modules
    modules = {}
    modules.merge!(expedients: 'assignment')
    modules.merge!(management_categories: 'assignment') if can?(:manage, ManagementCategory)
    modules.merge!(entry_ways:   'input') if can?(:manage, EntryWay)
    modules.merge!(institutions:   'location_city') if can?(:update, Institution)
    modules.merge!(branches:   'location_city') if can?(:update, Branch)
    modules.merge!(users:   'people') if can?(:read, User)
    modules.merge!(statistics:   'pie_chart') if can?(:read, :statistic)
    return modules
  end
  helper_method :modules

end

# :ruby
#   admin_modules = {
#     expedients:   'assignment',
#     management_categories:   'assignment',
#     entry_ways:   'input',
#     institutions: 'location_city',
#     users:        'people',
#     statistics:   'pie_chart',
#     documents:    'library_books',
#   }
#
#   oir_modules = {
#     expedients: 'assignment',
#     management_categories: 'assignment',
#     entry_ways: 'input',
#     users:      'people',
#     statistics: 'pie_chart',
#     documents:  'library_books',
#   }
#
#   modules = current_user.admin? ? admin_modules : oir_modules
