require 'prawn'
class ExpedientPdf < Prawn::Document
	def initialize(expedient, management = false)
		super(top_margin: 70)
		self.font_families.update("roboto" => {:normal      => "#{Rails.root}/public/Roboto-Regular.ttf",
																					 :bold        => "#{Rails.root}/public/Roboto-Bold.ttf",
																					 :italic      => "#{Rails.root}/public/Roboto-Italic.ttf",
																					 :bold_italic => "#{Rails.root}/public/Roboto-BoldItalic.ttf"})
    font "roboto"
		@expedient = expedient
		formatted_text [ { :text => "Expediente No. #{@expedient.code}", :styles => [:bold], size: 15 } ]
		text_content
		if management
			management_content
			if @expedient.closed?
				@closed_info = @expedient.events.where(state: 'closed').first
				resolution_info
			end
		end
	end

	def text_content
		text "\n<b>Código de referencía</b>: " + (@expedient.reference_code || '') + "\n\n", :inline_format => true
		text "<b>Fecha de ingreso</b>: " + (@expedient.received_at.try(:strftime, '%d/%m/%Y') || '') + "\n\n", :inline_format => true
		text "<b>Vía de ingreso</b>: " + (@expedient.kind) + "\n\n", :inline_format => true
		text "<b>Contacto</b>: " + (@expedient.contact.present? ? @expedient.contact : 'Anónimo') + "\n\n", :inline_format => true
		text "<b>Correo electrónico</b>: " + (@expedient.email.present? ? @expedient.email : 'N/A') + "\n\n", :inline_format => true
		text "<b>Teléfono</b>: " + (@expedient.phone.present? ? @expedient.phone : 'N/A') + "\n\n", :inline_format => true
		text "<b>Edad</b>: " + (@expedient.age.to_s) + "\n\n", :inline_format => true
		text "<b>Sexo</b>: " + (@expedient.gender_s) + "\n\n", :inline_format => true
		text "<b>Departamento</b>: " + (@expedient.department.try(:name) || 'N/A') + "\n\n", :inline_format => true
		text "<b>Municipio</b>: " + (@expedient.city.try(:name) || 'N/A') + "\n\n", :inline_format => true
		text "<b>Dependencia</b>: " + (@expedient.branch.try(:name) || 'N/A') + "\n\n", :inline_format => true
		text "<b>Comentario</b>: " + (@expedient.comment) + "\n\n", :inline_format => true
		@expedient.assets.each_with_index do |asset, index|
			#text asset.attachment_file_name + " (" + helper.number_to_human_size(asset.attachment_file_size) + ")"
			text asset.attachment_file_name.encode(Encoding.find('UTF-8'), {invalid: :replace, undef: :replace, replace: ''}) + " (" + helper.number_to_human_size(asset.attachment_file_size) + ")"
		end
	end


	def management_content
		formatted_text [ { :text => "Gestiones realizadas \n\n", :styles => [:bold, :underline], size: 14 } ]
		@expedient.managements.each_with_index do |management, index|
			unless management.new_record?
				text "\n\n" if index > 0
				text "<b><font size='16'>"+(I18n.l(management.created_at, format: :small))+"</font> " + \
					"<font size='12'>"+(I18n.l(management.created_at, format: :hour))+"</font></b>. " + \
					(management.assigned_names.present? \
						? (management.user.try(:name).to_s + " asignó a " + management.assigned_names + " una " + management.full_classification) \
						: (management.user.try(:name).to_s + " se asigno a si mismo un " + management.full_classification)) \
					, :inline_format => true
				text management.comment
				management.assets.each_with_index do |asset, index|
					text "\n" if index == 0
					text asset.attachment_file_name + " (" + helper.number_to_human_size(asset.attachment_file_size) + ")"
				end
				management.comments.each do |comment|
					text "\n<i>" + comment.user.try(:name).to_s + "</i> comento " + I18n.l(comment.created_at, format: :resume) + \
						"\n" + comment.comment, inline_format: true, indent_paragraphs: 10
					comment.assets.each_with_index do |asset, index|
						text asset.attachment_file_name + " (" + helper.number_to_human_size(asset.attachment_file_size) + ")", indent_paragraphs: 10
					end
				end
			end
		end
	end

	def resolution_info
		text "\n\n<b><font size='16'>"+(I18n.l(@closed_info.created_at, format: :small))+"</font> " + \
			"<font size='12'>"+(I18n.l(@closed_info.created_at, format: :hour))+"</font></b>. " + \
			"<b><font size='14'>Resolución del caso</font></b>" \
			, :inline_format => true
		if @expedient.classification_type
			text "<b>Clasificación:</b> #{I18n.t(@expedient.classification_type, scope: 'activerecord.enum.expedient.classification_type')}", :inline_format => true
		end
		text "\n<b>#{@closed_info.user.try(:name)}</b> cerro el caso con la siguiente justificación:", :inline_format => true
		text "\n#{@closed_info.justification}"
	end

	private
	def helper
		@helper ||= Class.new do
			include ActionView::Helpers::NumberHelper
		end.new
	end
end
