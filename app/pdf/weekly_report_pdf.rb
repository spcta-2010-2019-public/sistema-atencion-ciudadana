require 'prawn'
class WeeklyReportPdf < Prawn::Document
	def initialize
		super(top_margin: 30)
		self.font_families.update("roboto" => {:normal      => "#{Rails.root}/public/Roboto-Regular.ttf",
																					 :bold        => "#{Rails.root}/public/Roboto-Bold.ttf",
																					 :italic      => "#{Rails.root}/public/Roboto-Italic.ttf",
																					 :bold_italic => "#{Rails.root}/public/Roboto-BoldItalic.ttf"})
		font "roboto"
		@last_week = Date.current - 1.week
		@expedients = Expedient.includes(:institution).where(received_at: @last_week.beginning_of_week.beginning_of_day .. @last_week.end_of_week.end_of_day).reorder(received_at: :asc)
		draw_headers
		draw_generals
		draw_resume_institutions
		draw_resume_states
		draw_resume_of_attentions
		draw_details
	end

	def draw_headers
		formatted_text [ { :text => 'Sistema de Atención Ciudadana', :styles => [:bold], size: 14 } ]
		formatted_text [ { :text => 'Secretaría de Participación, Transparencia y Anticorrupción', :styles => [:bold], size: 14 } ]
		formatted_text [ { :text => "Reporte generado el #{I18n.l(Time.current, format: :resume)}", :styles => [:bold], size: 12 } ]
	end

	def draw_generals
		text "\nCasos recibidos durante la semana del #{I18n.l(@last_week.beginning_of_week, format: :short)} al #{I18n.l(@last_week.end_of_week, format: :short)}: <b>#{@expedients.size}</b>", inline_format: true
	end

	# Table 1
	def draw_resume_institutions
		text "\n\nTabla 1. Instituciones que recibieron casos (<b>#{@expedients.pluck(:institution_id).uniq.size}</b>)", inline_format: true
		data = [[
			'<b>Institución</b>',
			'<b>Cantidad de casos</b>',
		]]
		hash = @expedients.group_by{|e| e.institution_name}.map{|k, v| [k, v.size]}.to_h
		Hash[hash.sort_by{|k, v| v}.reverse].each do |k, v|
			data += [[
				k,v
			]]
		end
		data += [[
			'<b>Total</b>',
			"<b>#{hash.values.sum rescue 0}</b>"
		]]
		table(data, header: true, cell_style: { inline_format: true })
	end

	# Table 2
	def draw_resume_states
		text "\nTabla 2. Casos por estado", inline_format: true
		data = [[
			'<b>Estado</b>',
			'<b>Cantidad de casos</b>',
		]]
		hash = @expedients.group_by{|e| e.state}.map{|k, v| [k, v.size]}.to_h
		Hash[hash.sort_by{|k, v| v}.reverse].each do |k, v|
			data += [[
				Expedient::STATE[k],v
			]]
		end
		data += [[
			'<b>Total</b>',
			"<b>#{hash.values.sum rescue 0}</b>"
		]]
		table(data, header: true, cell_style: { inline_format: true })
	end

	# Table 3
	def draw_resume_of_attentions
		failed_communication_expedient_ids = Event.where(state: 'failed_communication_attempt', eventable_id: @expedients.pluck(:id)).pluck(:eventable_id).uniq
		text "\nTabla 3. Resumen de atenciones", inline_format: true
		data = [[
			'<b></b>',
			'<b>Cantidad de casos</b>',
		]]
		# Anonymous
		data += [[
			'Casos anónimos', @expedients.where(anonymous: true).count
		]]
		# Less than 48 hours
		data += [[
			'Casos atendidos en menos de 48 horas', @expedients.where('contact_business_days <= ?', 2).count
		]]
		# Without contact
		data += [[
			'Casos en los que no se hizo gestión por no poderse contactar con el ciudadano', @expedients.where(classification_type: 'without_info').count
		]]
		# Without management
		data += [[
			'Casos sin gestión', @expedients.where(state: 'new').where.not(id: failed_communication_expedient_ids).count
		]]
		data += [[
			'<b>Total</b>',
			"<b>#{@expedients.count}</b>"
		]]
		table(data, header: true, cell_style: { inline_format: true })
	end

	def draw_details
		text "\nTabla 4. Detalle de los casos", inline_format: true
		data = [[
			'<b>Institución</b>',
			'<b>Dependencia</b>',
			'<b>Código de referencia</b>',
			'<b>Nombre</b>',
			'<b>Resumen del caso</b>',
			'<b>Estado</b>'
		]]
		@expedients.each do |exp|
			data += [[
				exp.institution_name,
				exp.branch_name || 'N/A',
				exp.reference_code,
				exp.anonymous ? 'Anónimo' : exp.contact,
				exp.comment,
				exp.state_s
			]]
		end
		table(data, :header => true, :cell_style => { :inline_format => true })
	end

	private
	def helper
		@helper ||= Class.new do
			include ActionView::Helpers::NumberHelper
		end.new
	end
end
