require 'prawn'
class DailyReportPdf < Prawn::Document
	def initialize
		super(top_margin: 30)
		self.font_families.update("roboto" => {:normal      => "#{Rails.root}/public/Roboto-Regular.ttf",
																					 :bold        => "#{Rails.root}/public/Roboto-Bold.ttf",
																					 :italic      => "#{Rails.root}/public/Roboto-Italic.ttf",
																					 :bold_italic => "#{Rails.root}/public/Roboto-BoldItalic.ttf"})
		font "roboto"
		@last_day = Date.current - 1.day
		@expedients = Expedient.where(received_at: @last_day.beginning_of_day .. @last_day.end_of_day).reorder(received_at: :asc)
		draw_headers
		draw_generals
		draw_details
	end

	def draw_headers
		formatted_text [ { :text => 'Sistema de Atención Ciudadana', :styles => [:bold], size: 14 } ]
		formatted_text [ { :text => 'Secretaría de Participación, Transparencia y Anticorrupción', :styles => [:bold], size: 14 } ]
		formatted_text [ { :text => "Reporte generado el #{I18n.l(Time.current, format: :resume)}", :styles => [:bold], size: 12 } ]
	end

	def draw_generals
		text "\n\nCasos recibidos el día: <b>#{I18n.l(@last_day, format: :short)}</b>", inline_format: true
		text "Instituciones que recibieron casos: <b>#{@expedients.pluck(:institution_id).uniq.size}</b>", inline_format: true
	end

	def draw_details
		text "\n\n", inline_format: true
		data = [[
			'<b>Institución</b>',
			'<b>Dependencia</b>',
			'<b>Código de referencia</b>',
			'<b>Nombre</b>',
			'<b>Resumen del caso</b>',
			'<b>Estado</b>'
		]]
		@expedients.each do |exp|
			data += [[
				exp.institution_name,
				exp.branch_name || 'N/A',
				exp.reference_code,
				exp.anonymous ? 'Anónimo' : exp.contact,
				exp.comment,
				exp.state_s
			]]
		end
		table(data, :header => true, :cell_style => { :inline_format => true })
	end

	private
	def helper
		@helper ||= Class.new do
			include ActionView::Helpers::NumberHelper
		end.new
	end
end
