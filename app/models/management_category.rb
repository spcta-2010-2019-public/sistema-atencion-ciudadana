class ManagementCategory < ApplicationRecord
  belongs_to :institution
  has_many :managements, :dependent => :restrict_with_error

  validates :name, :kind, presence: true

  def institution_name
    institution.try(:name)
  end

  def kind_s
    Management::KIND[kind]
  end

  def full_name
    "#{Management::KIND[kind]} / #{name}"
  end

  def self.grouped_options institution_id
    registers = where(institution_id: institution_id).order(:kind, :name)
    return registers.group_by{|o| o.kind}.collect{|k,v| [Management::KIND[k], v.collect{|v| [v.name, v.id]}]}
  end
end
