class Whatsapp < ApplicationRecord
  validates :phone, :message, :kind, presence: true
end
