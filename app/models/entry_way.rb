class EntryWay < ApplicationRecord
  belongs_to :institution

  enum classification: [:own, :ga, :email, :phone, :social_network, :written_media, :other]

  validates :classification, :name, presence: true

  def institution_name
    institution.try(:name)
  end

  def classification_s
    EntryWay.human_attribute_name("classification.#{classification}")
  end
end
