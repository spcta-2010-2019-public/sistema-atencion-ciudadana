class Asset < ApplicationRecord
  belongs_to :assetable, polymorphic: true

  has_attached_file :attachment
  do_not_validate_attachment_file_type :attachment
end
