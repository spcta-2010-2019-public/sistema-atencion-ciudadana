class CalendarEvent < ApplicationRecord
  belongs_to :institution_calendar

  validates :institution_calendar_id, :kind, :name, :start_at, presence: true

  #before_validation :ensure_kind

  enum kind: [:holiday, :work]

  #def ensure_kind
  #  self.kind = 'holiday' unless kind.present?
  #end
end
