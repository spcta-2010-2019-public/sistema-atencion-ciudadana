class Comment < ApplicationRecord
  belongs_to :management, touch: true
  belongs_to :user
  has_many :assets, as: :assetable, dependent: :destroy

  after_create :notify_users

  def notify_users
    unless Rails.env.development?
      unless management.closed?
        User.where(id: [management.assigned_ids, management.user_id].flatten).where.not(id: user_id).each do |related_user|
          ApplicationMailer.notify_new_management_comment(self, related_user).deliver_now
        end
      end
    end
  end
end
