class InstitutionCalendar < ApplicationRecord

  belongs_to :institution
  has_many :calendar_events, dependent: :destroy

  # Valitadions
  validates :name, presence: true

  # Delegates
  delegate :name, to: :institution, prefix: true, allow_nil: true

end
