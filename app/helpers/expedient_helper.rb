module ExpedientHelper
  def expedient_state_for_filter
    Expedient::STATE.map{ |k,v| {id: k, title: v} }.to_json
  end
end
