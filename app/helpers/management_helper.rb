module ManagementHelper
  def management_kind_for_filter
    Management::KIND.map{ |k, v| {id: k, title: v} }.to_json
  end
end
