module EntryWayHelper
  def entry_way_attribute_classifications
    Hash[EntryWay.classifications.map { |k,v| [k, EntryWay.human_attribute_name("classification.#{k}")] }]
  end

  def entry_way_for_filter
    EntryWay.classifications.map{ |k,v| {id: v, title: EntryWay.human_attribute_name("classification.#{k}")} }.to_json
  end
end
