class ApplicationMailer < ActionMailer::Base
  default from: 'no-reply@atencionciudadana.gob.sv'
  layout 'mailer'

  def notify_new_expedient(expedient)
    @expedient    = expedient
    @institution  = @expedient.institution

    if Rails.env.development?
      to  = 'Henry Díaz <henry@gobiernoabierto.gob.sv>'
    else
      mail_recipes = User.left_joins(:roles, :institutions, :branches)
                    .where('roles.name = ? AND institutions.id = ?', 'oir', @expedient.institution_id)
                    .or(
                      User.left_joins(:roles, :institutions, :branches)
                      .where('roles.name = ? AND branches.id = ?', 'oir_local', @expedient.branch_id)
                    )
      to  = mail_recipes.pluck(:email)
    end

    mail to: to,
         subject: 'Nuevo expediente de Atención Ciudadana'
  end

  def notify_new_management(management, user)
    @management = management
    @user = user
    if Rails.env.development?
      to  = 'Henry Díaz <henry@gobiernoabierto.gob.sv>'
    else
      to  = @user.email
    end
    mail to: to,
         subject: 'Solicitud de nueva gestión de Atención Ciudadana'
  end

  def notify_new_management_comment(comment, user)
    @comment = comment
    @user = user
    if Rails.env.development?
      to  = 'Henry Díaz <henry@gobiernoabierto.gob.sv>'
    else
      to  = @user.email
    end
    mail to: to,
         subject: 'Nuevo comentario en gestión de Atención Ciudadana'
  end
end
