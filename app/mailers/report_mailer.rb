class ReportMailer < ApplicationMailer
  default from: 'no-reply@atencionciudadana.gob.sv'
  layout 'mailer'

  def daily_report
    @today = Date.current
    report = DailyReportPdf.new
    attachments["report_#{@today.strftime('%d_%m_%Y')}.pdf"] = { mime_type: 'application/pdf', content: report.render }
    mail to: ENV['DAILY_REPORT_EMAILS'],
         subject: "Reporte SAC #{I18n.l(@today, format: :short)}"
  end

  def weekly_report
    @today = Date.current
    report = WeeklyReportPdf.new
    attachments["weekly_report.pdf"] = { mime_type: 'application/pdf', content: report.render }
    mail to: ENV['DAILY_REPORT_EMAILS'],
         subject: "Reporte semanal Sistema de Atención Ciudadana"
  end
end
