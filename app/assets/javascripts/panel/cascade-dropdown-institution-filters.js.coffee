class @CascadeDropdownInstitutionFilters

  constructor: (dropdown_id, target_id, object, extras = null) ->

    @dropdown = $ "#" + dropdown_id
    @dropdown.on "change", =>

      @clear_targets()
      unless @dropdown.val() == ""
        @execute_ajax()

  clear_targets: ->
    @targets = ['q_entry_way_id_eq', 'q_managements_assigned_ids_cont', 'q_by_aa', 'q_branch_id_eq', 'q_by_tags']

    $.each(@targets, (i, id) =>
      target = $ "#" + id
      target
        .find("option")
        .remove()
      ($("<option />").text('Seleccione').val('')).appendTo(target)
    )

  execute_ajax: =>
    root = "/panel/statistics/#{@dropdown.val()}"

    urls_targets = [['/entryways.json', 'q_entry_way_id_eq'],
                    ['/managementsassigned.json', 'q_managements_assigned_ids_cont'],
                    ['/administrativearea.json', 'q_by_aa'],
                    ['/branches.json', 'q_branch_id_eq'],
                    ['/tags.json', 'q_by_tags']]

    $.each(urls_targets, (i, ut) =>
      target = $ "#" + ut[1]
      url    = root + ut[0]

      $.getJSON url, (data) =>
        $.each(data, (i, val) =>
          $("<option />")
            .val(i)
            .text(val)
            .appendTo(target)
        )
    )
