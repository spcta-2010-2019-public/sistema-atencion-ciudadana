$(function(){
  $('#datepicker').datepick({
    multiSelect: 999,
    prevText: '< M',
    nextText: 'M >',
    todayText: 'M yyyy',
    commandsAsDateFormat: true,
    altField: '#days',
    altFormat: 'yyyy-mm-dd',
    useMouseWheel: false
  });
  // Init trigger
  initValues();
});
function initValues(){
  var start_dates = $('#days').val() ? $('#days').val().split(',') : null;
  if(start_dates) {
    for(i=0; i<start_dates.length; i++){
      date_tz = new Date(start_dates[i]);
      userTimezoneOffset = date_tz.getTimezoneOffset() * 60000;

      start_dates[i] = new Date(date_tz.getTime() + userTimezoneOffset);
    }
    $('#datepicker').datepick('setDate', start_dates);
  }
}
