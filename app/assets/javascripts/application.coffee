#= require jquery
#= require jquery_ujs
#= require dropzone
#= require chartkick

regexWordCount = /\s+/gi

check_first_step = ->
  if $('#expedient_comment').length > 0 && $('#expedient_institution_id').length > 0
    parent = $('#expedient_comment').closest('section')
    if $('#expedient_comment').val().trim().length > 0 && $('#expedient_institution_id').val()
      $('a.next-section', parent).removeClass 'disabled'
    else
      $('a.next-section', parent).addClass 'disabled'

hashtable =
  hash: {}
  exists: (key) ->
    @hash.hasOwnProperty key
  get: (key) ->
    if @exists(key) then @hash[key] else null
  put: (key, value) ->
    @hash[key] = value
    return

$ ->

  $('nav a').on 'click', (e) ->
    e.stopPropagation() unless $(this).hasClass('active')

  $('nav').on 'click', (e) ->
    if $(this).find('a:hidden').length > 0 || $(this).hasClass('active')
      e.preventDefault()
      $(this).toggleClass('active')
      $('body').toggleClass('overlayed')

  ##
  # Dropzone configure
  Dropzone.autoDiscover = false
  $('.dropzone').each ->
    dropUrl = $(this).attr('action')
    previewContainer = $(this).data('preview')
    $(this).dropzone
      autoProcessQueue: false
      uploadMultiple: true
      parallelUploads: 100
      maxFiles: 100
      previewsContainer: previewContainer
      url: dropUrl
      addRemoveLinks: true
      init: ->
        myDropzone = this
        # First change the button to actually tell Dropzone to process the queue.
        @element.querySelector('button[type=submit]').addEventListener 'click', (e) ->
          if myDropzone.getQueuedFiles().length > 0
            # Make sure that the form isn't actually being sent.
            e.preventDefault()
            e.stopPropagation()
            myDropzone.processQueue()
          return
        @on 'sendingmultiple', ->
          return
        @on 'successmultiple', (files, response) ->
          if response.code == 200
            window.location = response.redirect_url
            setTimeout (->
              window.location.reload()
            ), 500
          return
        @on 'errormultiple', (files, response) ->
          return
        return

  $('.next-section').click ->
    return if $(this).hasClass 'disabled'
    parent = $(this).closest('section')
    parent.hide()
    parent.next('section').show()

  $('.prev-section').click ->
    parent = $(this).closest('section')
    parent.hide()
    parent.prev('section').show()

  $('#expedient_comment').keyup ->
    check_first_step()
  $('#expedient_institution_id').change ->
    check_first_step()
  check_first_step()


  $('select[data-dependent]').each (index) ->
    dependent = $('.' + $(@).data('target'))
    hashtable.put $(@).data('child'), dependent.html()
    $('optgroup', dependent).remove()
    if $(@).data('hidden')
      dependent.closest('div').hide()
    $(@).on 'change', ->
      hidden = $(@).data('hidden')
      type = $('option:selected', this).text()
      options = hashtable.get($(@).data('child'))
      values = $(options).filter('optgroup[label=\'' + type + '\']').html()
      dependent.find('option:gt(0)').remove()
      if values
        dependent.append values
        if hidden
          dependent.closest('div').show('slow')
      else
        if hidden
          dependent.closest('div').hide('slow')
      return
    $(this).trigger 'change'
    return

  $('.copy-code').click ->
    div = document.createRange()
    div.setStartBefore(document.getElementById("my_code"))
    div.setEndAfter(document.getElementById("my_code"))

    window.getSelection().addRange(div);
    document.execCommand("copy")

  $('.print-code').click ->
    window.print()
