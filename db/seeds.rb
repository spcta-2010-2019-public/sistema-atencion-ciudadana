# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create(
#  City.create!(state_id: { n name:ame: 'Star Wars' }, { name: 'Lord of the Rings' )
#   Character.create(name: 'Luke', movie: movies.first)


# require 'csv'
# CSV.open("#{Rails.root.to_s}/db/compilado_estadistico.csv", "w") do |csv|
#   csv << [
#     "Institución",
#     "Cantidad de casos",
#     "Casos nuevos",
#     "Casos en proceso",
#     "Casos cerrados",
#     "Casos redireccionados",
#     "Quejas",
#     "Reclamos",
#     "Avisos",
#     "Sugerencia",
#     "Felicitación",
#     "Petición o solicitud",
#     "Tiempo promedio de respuesta",
#     "Subcategorías más frecuentes"
#   ]
#   Institution.order(:name).each do |i|
#     expedients = Expedient.where(institution_id: i.id)
#     csv << [
#       i.name,
#       expedients.count,
#       expedients.where(state: 'new').count,
#       expedients.where(state: 'process').count,
#       expedients.where(state: 'closed').count,
#       expedients.where(state: 'redirected').count,
#       Management.where(expedient_id: expedients.pluck(:id), kind: 'complaint').count,
#       Management.where(expedient_id: expedients.pluck(:id), kind: 'claim').count,
#       Management.where(expedient_id: expedients.pluck(:id), kind: 'notice').count,
#       Management.where(expedient_id: expedients.pluck(:id), kind: 'suggestion').count,
#       Management.where(expedient_id: expedients.pluck(:id), kind: 'congratulation').count,
#       Management.where(expedient_id: expedients.pluck(:id), kind: 'request').count,
#       ("#{(expedients.where(state: 'closed').map(&:response_time).sum / expedients.where(state: 'closed').count).round(2)} horas" rescue 'N/A'),
#       Management.select('management_category_id, sum(weight) as total').where(expedient_id: expedients.pluck(:id)).where.not(management_category_id: nil).group(:management_category_id).order('total DESC').limit(5).map{|o| "#{o.total} gestiones de #{ManagementCategory.find(o.management_category_id).try(:name)}"}.join(', ')
#     ]
#   end
# end


# if State.count == 0
#   State.create!(name: "Ahuachapán")
#   State.create!(name: "Cabañas")
#   State.create!(name: "Chalatenango")
#   State.create!(name: "Cuscatlán")
#   State.create!(name: "La Libertad")
#   State.create!(name: "La Paz")
#   State.create!(name: "La Unión")
#   State.create!(name: "Morazán")
#   State.create!(name: "San Miguel")
#   State.create!(name: "San Salvador")
#   State.create!(name: "San Vicente")
#   State.create!(name: "Santa Ana")
#   State.create!(name: "Sonsonate")
#   State.create!(name: "Usulután")
# end

# if City.count == 0
#   City.create!(state_id: 1,  name:"Ahuachapán")
#   City.create!(state_id: 1,  name:"Jujutla")
#   City.create!(state_id: 1,  name:"Atiquizaya")
#   City.create!(state_id: 1,  name:"Concepción de Ataco")
#   City.create!(state_id: 1,  name:"El Refugio")
#   City.create!(state_id: 1,  name:"Guaymango")
#   City.create!(state_id: 1,  name:"Apaneca")
#   City.create!(state_id: 1,  name:"San Francisco Menéndez")
#   City.create!(state_id: 1,  name:"San Lorenzo")
#   City.create!(state_id: 1,  name:"San Pedro Puxtla")
#   City.create!(state_id: 1,  name:"Tacuba")
#   City.create!(state_id: 1,  name:"Turín")
#   City.create!(state_id: 2,  name:"Cinquera")
#   City.create!(state_id: 2,  name:"Villa Dolores")
#   City.create!(state_id: 2,  name:"Guacotecti")
#   City.create!(state_id: 2,  name:"Ilobasco")
#   City.create!(state_id: 2,  name:"Jutiapa")
#   City.create!(state_id: 2,  name:"San Isidro")
#   City.create!(state_id: 2,  name:"Sensuntepeque")
#   City.create!(state_id: 2,  name:"Tejutepeque")
#   City.create!(state_id: 2,  name:"Victoria")
#   City.create!(state_id: 3,  name:"Agua Caliente")
#   City.create!(state_id: 3,  name:"Arcatao")
#   City.create!(state_id: 3,  name:"Azacualpa")
#   City.create!(state_id: 3,  name:"Chalatenango")
#   City.create!(state_id: 3,  name:"Citalá")
#   City.create!(state_id: 3,  name:"Comalapa")
#   City.create!(state_id: 3,  name:"Concepción Quezaltepeque")
#   City.create!(state_id: 3,  name:"Dulce Nombre de María")
#   City.create!(state_id: 3,  name:"El Carrizal")
#   City.create!(state_id: 3,  name:"El Paraíso")
#   City.create!(state_id: 3,  name:"La Laguna")
#   City.create!(state_id: 3,  name:"La Palma")
#   City.create!(state_id: 3,  name:"La Reina")
#   City.create!(state_id: 3,  name:"Las Vueltas")
#   City.create!(state_id: 3,  name:"Nombre de Jesús")
#   City.create!(state_id: 3,  name:"Nueva Concepción")
#   City.create!(state_id: 3,  name:"Nueva Trinidad")
#   City.create!(state_id: 3,  name:"Ojos de Agua")
#   City.create!(state_id: 3,  name:"Potonico")
#   City.create!(state_id: 3,  name:"San Antonio de la Cruz")
#   City.create!(state_id: 3,  name:"San Antonio Los Ranchos")
#   City.create!(state_id: 3,  name:"San Fernando")
#   City.create!(state_id: 3,  name:"San Francisco Lempa")
#   City.create!(state_id: 3,  name:"San Francisco Morazán")
#   City.create!(state_id: 3,  name:"San Ignacio")
#   City.create!(state_id: 3,  name:"San Isidro Labrador")
#   City.create!(state_id: 3,  name:"San José Cancasque")
#   City.create!(state_id: 3,  name:"San José Las Flores")
#   City.create!(state_id: 3,  name:"San Luis del Carmen")
#   City.create!(state_id: 3,  name:"San Miguel de Mercedes")
#   City.create!(state_id: 3,  name:"San Rafael")
#   City.create!(state_id: 3,  name:"Santa Rita")
#   City.create!(state_id: 3,  name:"Tejutla")
#   City.create!(state_id: 4,  name:"Candelaria")
#   City.create!(state_id: 4,  name:"Cojutepeque")
#   City.create!(state_id: 4,  name:"El Carmen")
#   City.create!(state_id: 4,  name:"El Rosario")
#   City.create!(state_id: 4,  name:"Monte San Juan")
#   City.create!(state_id: 4,  name:"Oratorio de Concepción")
#   City.create!(state_id: 4,  name:"San Bartolomé Perulapía")
#   City.create!(state_id: 4,  name:"San Cristóbal")
#   City.create!(state_id: 4,  name:"San José Guayabal")
#   City.create!(state_id: 4,  name:"San Pedro Perulapán")
#   City.create!(state_id: 4,  name:"San Rafael Cedros")
#   City.create!(state_id: 4,  name:"San Ramón")
#   City.create!(state_id: 4,  name:"Santa Cruz Analquito")
#   City.create!(state_id: 4,  name:"Santa Cruz Michapa")
#   City.create!(state_id: 4,  name:"Suchitoto")
#   City.create!(state_id: 4,  name:"Tenancingo")
#   City.create!(state_id: 5,  name:"Antiguo Cuscatlán")
#   City.create!(state_id: 5,  name:"Chiltiupán")
#   City.create!(state_id: 5,  name:"Ciudad Arce")
#   City.create!(state_id: 5,  name:"Colón")
#   City.create!(state_id: 5,  name:"Comasagua")
#   City.create!(state_id: 5,  name:"Huizúcar")
#   City.create!(state_id: 5,  name:"Jayaque")
#   City.create!(state_id: 5,  name:"Jicalapa")
#   City.create!(state_id: 5,  name:"La Libertad")
#   City.create!(state_id: 5,  name:"Santa Tecla")
#   City.create!(state_id: 5,  name:"Nuevo Cuscatlán")
#   City.create!(state_id: 5,  name:"San Juan Opico")
#   City.create!(state_id: 5,  name:"Quezaltepeque")
#   City.create!(state_id: 5,  name:"Sacacoyo")
#   City.create!(state_id: 5,  name:"San José Villanueva")
#   City.create!(state_id: 5,  name:"San Matías")
#   City.create!(state_id: 5,  name:"San Pablo Tacachico")
#   City.create!(state_id: 5,  name:"Talnique")
#   City.create!(state_id: 5,  name:"Tamanique")
#   City.create!(state_id: 5,  name:"Teotepeque")
#   City.create!(state_id: 5,  name:"Tepecoyo")
#   City.create!(state_id: 5,  name:"Zaragoza")
#   City.create!(state_id: 6,  name:"Cuyultitán")
#   City.create!(state_id: 6,  name:"El Rosario")
#   City.create!(state_id: 6,  name:"Jerusalén")
#   City.create!(state_id: 6,  name:"Mercedes La Ceiba")
#   City.create!(state_id: 6,  name:"Olocuilta")
#   City.create!(state_id: 6,  name:"Paraíso de Osorio")
#   City.create!(state_id: 6,  name:"San Antonio Masahuat")
#   City.create!(state_id: 6,  name:"San Emigdio")
#   City.create!(state_id: 6,  name:"San Francisco Chinameca")
#   City.create!(state_id: 6,  name:"San Juan Nonualco")
#   City.create!(state_id: 6,  name:"San Juan Talpa")
#   City.create!(state_id: 6,  name:"San Juan Tepezontes")
#   City.create!(state_id: 6,  name:"San Luis La Herradura")
#   City.create!(state_id: 6,  name:"San Luis Talpa")
#   City.create!(state_id: 6,  name:"San Miguel Tepezontes")
#   City.create!(state_id: 6,  name:"San Pedro Masahuat")
#   City.create!(state_id: 6,  name:"San Pedro Nonualco")
#   City.create!(state_id: 6,  name:"San Rafael Obrajuelo")
#   City.create!(state_id: 6,  name:"Santa María Ostuma")
#   City.create!(state_id: 6,  name:"Santiago Nonualco")
#   City.create!(state_id: 6,  name:"Tapalhuaca")
#   City.create!(state_id: 6,  name:"Zacatecoluca")
#   City.create!(state_id: 7,  name:"Anamorós")
#   City.create!(state_id: 7,  name:"Bolívar")
#   City.create!(state_id: 7,  name:"Concepción de Oriente")
#   City.create!(state_id: 7,  name:"Conchagua")
#   City.create!(state_id: 7,  name:"El Carmen")
#   City.create!(state_id: 7,  name:"El Sauce")
#   City.create!(state_id: 7,  name:"Intipucá")
#   City.create!(state_id: 7,  name:"La Unión")
#   City.create!(state_id: 7,  name:"Lislique")
#   City.create!(state_id: 7,  name:"Meanguera del Golfo")
#   City.create!(state_id: 7,  name:"Nueva Esparta")
#   City.create!(state_id: 7,  name:"Pasaquina")
#   City.create!(state_id: 7,  name:"Polorós")
#   City.create!(state_id: 7,  name:"San Alejo")
#   City.create!(state_id: 7,  name:"San José")
#   City.create!(state_id: 7,  name:"Santa Rosa de Lima")
#   City.create!(state_id: 7,  name:"Yayantique")
#   City.create!(state_id: 7,  name:"Yucuayquín")
#   City.create!(state_id: 8,  name:"Arambala")
#   City.create!(state_id: 8,  name:"Cacaopera")
#   City.create!(state_id: 8,  name:"Chilanga")
#   City.create!(state_id: 8,  name:"Corinto")
#   City.create!(state_id: 8,  name:"Delicias de Concepción")
#   City.create!(state_id: 8,  name:"El Divisadero")
#   City.create!(state_id: 8,  name:"El Rosario")
#   City.create!(state_id: 8,  name:"Gualococti")
#   City.create!(state_id: 8,  name:"Guatajiagua")
#   City.create!(state_id: 8,  name:"Joateca")
#   City.create!(state_id: 8,  name:"Jocoaitique")
#   City.create!(state_id: 8,  name:"Jocoro")
#   City.create!(state_id: 8,  name:"Lolotiquillo")
#   City.create!(state_id: 8,  name:"Meanguera")
#   City.create!(state_id: 8,  name:"Osicala")
#   City.create!(state_id: 8,  name:"Perquín")
#   City.create!(state_id: 8,  name:"San Carlos")
#   City.create!(state_id: 8,  name:"San Fernando")
#   City.create!(state_id: 8,  name:"San Francisco Gotera")
#   City.create!(state_id: 8,  name:"San Isidro")
#   City.create!(state_id: 8,  name:"San Simón")
#   City.create!(state_id: 8,  name:"Sensembra")
#   City.create!(state_id: 8,  name:"Sociedad")
#   City.create!(state_id: 8,  name:"Torola")
#   City.create!(state_id: 8,  name:"Yamabal")
#   City.create!(state_id: 8,  name:"Yoloaiquín")
#   City.create!(state_id: 9,  name:"Carolina")
#   City.create!(state_id: 9,  name:"Chapeltique")
#   City.create!(state_id: 9,  name:"Chinameca")
#   City.create!(state_id: 9,  name:"Chirilagua")
#   City.create!(state_id: 9,  name:"Ciudad Barrios")
#   City.create!(state_id: 9,  name:"Comacarán")
#   City.create!(state_id: 9,  name:"El Tránsito")
#   City.create!(state_id: 9,  name:"Lolotique")
#   City.create!(state_id: 9,  name:"Moncagua")
#   City.create!(state_id: 9,  name:"Nueva Guadalupe")
#   City.create!(state_id: 9,  name:"Nuevo Edén de San Juan")
#   City.create!(state_id: 9,  name:"Quelepa")
#   City.create!(state_id: 9,  name:"San Antonio")
#   City.create!(state_id: 9,  name:"San Gerardo")
#   City.create!(state_id: 9,  name:"San Jorge")
#   City.create!(state_id: 9,  name:"San Luis de la Reina")
#   City.create!(state_id: 9,  name:"San Miguel")
#   City.create!(state_id: 9,  name:"San Rafael")
#   City.create!(state_id: 9,  name:"Sesori")
#   City.create!(state_id: 9,  name:"Uluazapa")
#   City.create!(state_id: 10, name: "Aguilares")
#   City.create!(state_id: 10, name: "Apopa")
#   City.create!(state_id: 10, name: "Ayutuxtepeque")
#   City.create!(state_id: 10, name: "Cuscatancingo")
#   City.create!(state_id: 10, name: "Delgado")
#   City.create!(state_id: 10, name: "El Paisnal")
#   City.create!(state_id: 10, name: "Guazapa")
#   City.create!(state_id: 10, name: "Ilopango")
#   City.create!(state_id: 10, name: "Mejicanos")
#   City.create!(state_id: 10, name: "Nejapa")
#   City.create!(state_id: 10, name: "Panchimalco")
#   City.create!(state_id: 10, name: "Rosario de Mora")
#   City.create!(state_id: 10, name: "San Marcos")
#   City.create!(state_id: 10, name: "San Martín")
#   City.create!(state_id: 10, name: "San Salvador")
#   City.create!(state_id: 10, name: "Santiago Texacuangos")
#   City.create!(state_id: 10, name: "Santo Tomás")
#   City.create!(state_id: 10, name: "Soyapango")
#   City.create!(state_id: 10, name: "Tonacatepeque")
#   City.create!(state_id: 11, name: "Apastepeque")
#   City.create!(state_id: 11, name: "Guadalupe")
#   City.create!(state_id: 11, name: "San Cayetano Istepeque")
#   City.create!(state_id: 11, name: "San Esteban Catarina")
#   City.create!(state_id: 11, name: "San Ildefonso")
#   City.create!(state_id: 11, name: "San Lorenzo")
#   City.create!(state_id: 11, name: "San Sebastián")
#   City.create!(state_id: 11, name: "Santa Clara")
#   City.create!(state_id: 11, name: "Santo Domingo")
#   City.create!(state_id: 11, name: "San Vicente")
#   City.create!(state_id: 11, name: "Tecoluca")
#   City.create!(state_id: 11, name: "Tepetitán")
#   City.create!(state_id: 11, name: "Verapaz")
#   City.create!(state_id: 12, name: "Candelaria de la Frontera")
#   City.create!(state_id: 12, name: "Chalchuapa")
#   City.create!(state_id: 12, name: "Coatepeque")
#   City.create!(state_id: 12, name: "El Congo")
#   City.create!(state_id: 12, name: "El Porvenir")
#   City.create!(state_id: 12, name: "Masahuat")
#   City.create!(state_id: 12, name: "Metapán")
#   City.create!(state_id: 12, name: "San Antonio Pajonal")
#   City.create!(state_id: 12, name: "San Sebastián Salitrillo")
#   City.create!(state_id: 12, name: "Santa Ana")
#   City.create!(state_id: 12, name: "Santa Rosa Guachipilín")
#   City.create!(state_id: 12, name: "Santiago de la Frontera")
#   City.create!(state_id: 12, name: "Texistepeque")
#   City.create!(state_id: 13, name: "Acajutla")
#   City.create!(state_id: 13, name: "Armenia")
#   City.create!(state_id: 13, name: "Caluco")
#   City.create!(state_id: 13, name: "Cuisnahuat")
#   City.create!(state_id: 13, name: "Izalco")
#   City.create!(state_id: 13, name: "Juayúa")
#   City.create!(state_id: 13, name: "Nahuizalco")
#   City.create!(state_id: 13, name: "Nahulingo")
#   City.create!(state_id: 13, name: "Salcoatitán")
#   City.create!(state_id: 13, name: "San Antonio del Monte")
#   City.create!(state_id: 13, name: "San Julián")
#   City.create!(state_id: 13, name: "Santa Catarina Masahuat")
#   City.create!(state_id: 13, name: "Santa Isabel Ishuatán")
#   City.create!(state_id: 13, name: "Santo Domingo")
#   City.create!(state_id: 13, name: "Sonsonate")
#   City.create!(state_id: 13, name: "Sonzacate")
#   City.create!(state_id: 14, name: "Alegría")
#   City.create!(state_id: 14, name: "Berlín")
#   City.create!(state_id: 14, name: "California")
#   City.create!(state_id: 14, name: "Concepción Batres")
#   City.create!(state_id: 14, name: "El Triunfo")
#   City.create!(state_id: 14, name: "Ereguayquín")
#   City.create!(state_id: 14, name: "Estanzuelas")
#   City.create!(state_id: 14, name: "Jiquilisco")
#   City.create!(state_id: 14, name: "Jucuapa")
#   City.create!(state_id: 14, name: "Jucuarán")
#   City.create!(state_id: 14, name: "Mercedes Umaña")
#   City.create!(state_id: 14, name: "Nueva Granada")
#   City.create!(state_id: 14, name: "Ozatlán")
#   City.create!(state_id: 14, name: "Puerto El Triunfo")
#   City.create!(state_id: 14, name: "San Agustín")
#   City.create!(state_id: 14, name: "San Buenaventura")
#   City.create!(state_id: 14, name: "San Dionisio")
#   City.create!(state_id: 14, name: "San Francisco Javier")
#   City.create!(state_id: 14, name: "Santa Elena")
#   City.create!(state_id: 14, name: "Santa María")
#   City.create!(state_id: 14, name: "Santiago de María")
#   City.create!(state_id: 14, name: "Tecapán")
#   City.create!(state_id: 14, name: "Usulután")
# end


# institution_id = 17
# ManagementCategory.where(institution_id: institution_id).destroy_all
# require 'csv'
# csv_path = "#{Rails.root.to_s}/db/tipologia_isss.csv"
# CSV.foreach(csv_path, headers: true) do |row|
#   begin
#     ManagementCategory.create(
#       institution_id: institution_id,
#       kind: row[0],
#       name: row[1],
#     )
#   rescue
#     puts "Error en la carga"
#   end
# end


# require 'csv'
# csv_path = "#{Rails.root.to_s}/db/centros_isss.csv"
# institution_id = 17
# CSV.foreach(csv_path, headers: false) do |row|
#   begin
#     Branch.create(
#       name: row[0],
#       institution_id: 17
#     )
#   rescue
#     puts "Error en la carga"
#   end
# end

# USUARIOS ADMINISTRADORES JRS.
# require 'csv'
# csv_path = "#{Rails.root.to_s}/db/usuarios_dependencias.csv"
# CSV.foreach(csv_path, headers: true) do |row|
#   # begin
#     if row[0].present? and row[1].present? and row[2].present? and row[3].present? and row[4].present?
#       # institution_id = row[0]
#       institution_id = 2
#       role = Role.where(name: 'oir_local')
#       b = Branch.where(institution_id: institution_id, name: row[1].strip).first_or_create
#       u = User.where(email: row[3].to_s.strip).first_or_initialize(name: row[2].to_s.strip)
#       u.password = row[4].to_s.strip
#       u.institution_ids = institution_id
#       u.branch_ids = b.id
#       u.role_ids = nil
#       u.roles = role
#       unless u.save
#         puts u.inspect
#         puts u.errors.full_messages
#       end
#     end
#   # rescue Exception => e
#   #   puts e.inspect
#   # end
# end

# USUARIOS ADMINISTRADORES
# require 'csv'
# csv_path = "#{Rails.root.to_s}/db/administradores.csv"
# CSV.foreach(csv_path, headers: true) do |row|
#   # begin
#     if row[0].present? and row[1].present? and row[2].present? and row[3].present? and row[4].present?
#       institution = Institution.where(acronym: row[0].to_s.strip).first_or_create(name: row[1].to_s.strip)
#       role = Role.where(name: 'oir')
#       u = User.where(email: row[3].to_s.strip).first_or_initialize(name: row[2].to_s.strip)
#       u.password = row[4].to_s.strip
#       u.institution_ids = institution.id
#       u.role_ids = nil
#       if u.save
#         u.roles = role
#       else
#         puts u.inspect
#         puts u.errors.full_messages
#       end
#     end
#   # rescue Exception => e
#   #   puts e.inspect
#   # end
# end

# CREAR GESTIONES CON CATEGORIAS/SUBCATEGORIAS
# require 'csv'
# csv_path = "#{Rails.root.to_s}/db/cats_and_subcats.csv"
# s_c = []
# CSV.foreach(csv_path, headers: true) do |row|
#   # begin
#     if row[0].present? and row[1].present? and row[2].present? and row[3].present?
#       exp = Expedient.where(id: row[0]).first
#       if exp
#         unless ['without_info','duplicity'].include?(exp.classification_type)
#           contact = exp.events.where(state: 'process').first
#           if contact.nil?
#             s_c << exp.id
#           else
#             created_at = contact.start_at + 15.minutes
#             # We create a management and a event
#             management_category_id = ManagementCategory.where(institution_id: exp.institution_id, kind: row[2].strip, name: row[3].strip).first.try(:id)
#             management = Management.new(kind: row[2].strip, management_category_id: management_category_id, comment: exp.comment.truncate(100))
#             management.expedient_id = exp.id
#             management.user_id = contact.user_id
#             management.state = 'process'
#             management.save
#             management.update_column(:created_at, created_at)
#             management.update_column(:updated_at, created_at)
#             event = management.events.create(state: 'process', user_id: contact.user_id, start_at: created_at, justification: 'Inicio del proceso')
#             event.update_column(:created_at, created_at)
#             event.update_column(:updated_at, created_at)
#
#             # We then close this management
#             closed_at = created_at + 15.minutes
#             event = management.events.new(start_at: closed_at, weight: 1, justification: 'Cierre del proceso', kind: management.kind, management_category_id: management.management_category_id )
#             event.state = 'closed'
#             event.user_id = contact.user_id
#             event.save
#             event.update_column(:created_at, closed_at)
#             event.update_column(:updated_at, closed_at)
#             management.state = event.state
#             management.weight = event.weight
#             management.save
#             management.update_column(:updated_at, closed_at)
#           end
#         end
#       else
#         puts "#{row[0]} no existe."
#       end
#     end
#   # rescue Exception => e
#   #   puts e.inspect
#   # end
# end
# puts "Sin contacto:"
# puts s_c.inspect

# CARGA DE POSITIVOS / NEGATIVOS
require 'csv'
csv_path = "#{Rails.root.to_s}/db/positivo_o_negativo_2.csv"
CSV.foreach(csv_path, headers: true) do |row|
  # begin
    if row[0].present? and row[1].present? and row[2].present?
      e = Expedient.where(id: row[0].strip.to_i).first
      if e
        if Expedient::RESOLUTION_TYPE.keys.include?(row[2].strip.to_s)
          e.update_column(:resolution_type, row[2].strip)
        elsif ["0","1"].include?(row[2].strip.to_s)
          e.update_column(:it_proceeds, row[2].strip.to_i)
        else
          puts "Option not found: #{row[2]}"
        end
      else
        puts "Expediente #{row[0]}, no encontrado."
      end
    else
      puts "No se encontraron datos."
    end
  # rescue Exception => e
  #   puts e.inspect
  # end
end
