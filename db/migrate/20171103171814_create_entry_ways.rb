class CreateEntryWays < ActiveRecord::Migration[5.0]
  def change
    create_table :entry_ways do |t|
      t.integer :classification, default: 0
      t.references :institution, index: true
      t.string :name
      t.timestamps
    end
  end
end
