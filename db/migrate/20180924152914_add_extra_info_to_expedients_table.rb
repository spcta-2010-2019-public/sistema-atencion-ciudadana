class AddExtraInfoToExpedientsTable < ActiveRecord::Migration[5.0]
  def change
    add_column :expedients, :happened_at, :timestamp, default: nil
    add_column :expedients, :place, :string
    add_column :expedients, :national_license_plate, :string
    add_column :expedients, :personnel_name, :string
    add_column :expedients, :process_name, :string
  end
end
