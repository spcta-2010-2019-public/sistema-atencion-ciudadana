class CreateEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :events do |t|
      t.references :user, index: true
      t.references :eventable, polymorphic: true, index: true
      t.timestamp :start_at
      t.text :justification
      t.string :state, default: 'new'
      t.timestamps
    end
  end
end
