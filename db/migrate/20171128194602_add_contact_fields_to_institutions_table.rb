class AddContactFieldsToInstitutionsTable < ActiveRecord::Migration[5.0]
  def change
    add_column :institutions, :address, :text
    add_column :institutions, :responsible_unit, :string
    add_column :institutions, :phone, :string
    add_column :institutions, :facebook, :string
    add_column :institutions, :twitter, :string
    add_column :institutions, :whatsapp, :string
    add_column :institutions, :email, :string
  end
end
