class AddCanEditFieldToInstitutionsAndBranchesTable < ActiveRecord::Migration[5.0]
  def change
    add_column :institutions, :can_edit, :boolean, default: false
    add_column :branches, :can_edit, :boolean, default: false
  end
end
