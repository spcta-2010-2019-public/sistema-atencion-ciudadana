class CreateExpedients < ActiveRecord::Migration[5.0]
  def change
    create_table :expedients do |t|
      t.references :entry_way, index: true
      t.string :contact, index: true
      t.string :phone
      t.string :email
      t.text :comment
      t.references :institution, index: true
      t.string :state, default: 'new', index: true
      t.string :correlative, index: true
      t.references :user, index: true
      t.datetime :received_at
      t.datetime :confirmed_at
      t.datetime :admitted_at
      t.string :reference_code, index: true
      t.references :expedient, index: true
      t.timestamps
    end
  end
end
