class CreateBranchesUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :branches_users, id: false do |t|
      t.references :branch, foreign_key: true
      t.references :user, foreign_key: true
    end
  end
end
