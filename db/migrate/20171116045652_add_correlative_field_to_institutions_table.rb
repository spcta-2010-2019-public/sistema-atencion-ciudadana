class AddCorrelativeFieldToInstitutionsTable < ActiveRecord::Migration[5.0]
  def change
    add_column :institutions, :correlative, :integer, default: 1
  end
end
