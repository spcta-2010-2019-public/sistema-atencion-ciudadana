class CreateWhatsapps < ActiveRecord::Migration[5.0]
  def change
    create_table :whatsapps do |t|
      t.string :kind, default: 'consulta'
      t.string :phone
      t.text :message
      t.timestamps
    end
  end
end
