class AddResponseToExpedients < ActiveRecord::Migration[5.0]
  def change
    add_column :expedients, :response, :boolean, default: false
  end
end
