class AddClasificationTypeToExpedients < ActiveRecord::Migration[5.0]
  def change
    add_column :expedients, :classification_type, :integer
  end
end
