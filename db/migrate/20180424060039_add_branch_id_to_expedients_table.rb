class AddBranchIdToExpedientsTable < ActiveRecord::Migration[5.0]
  def change
    add_column :expedients, :branch_id, :integer
    add_index :expedients, :branch_id
  end
end
