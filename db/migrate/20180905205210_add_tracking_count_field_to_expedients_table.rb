class AddTrackingCountFieldToExpedientsTable < ActiveRecord::Migration[5.0]
  def change
    add_column :expedients, :tracking_count, :integer, default: 0
  end
end
