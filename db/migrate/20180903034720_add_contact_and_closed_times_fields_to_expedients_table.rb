class AddContactAndClosedTimesFieldsToExpedientsTable < ActiveRecord::Migration[5.0]
  def change
    add_column :expedients, :contact_business_days, :integer
    add_column :expedients, :contact_business_hours, :float
    add_column :expedients, :closed_business_days, :integer
    add_column :expedients, :closed_business_hours, :float
  end
end
