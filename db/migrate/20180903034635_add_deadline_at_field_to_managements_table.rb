class AddDeadlineAtFieldToManagementsTable < ActiveRecord::Migration[5.0]
  def change
    add_column :managements, :deadline_at, :date
    add_column :managements, :closed_business_days, :integer
    add_column :managements, :closed_business_hours, :float
  end
end
