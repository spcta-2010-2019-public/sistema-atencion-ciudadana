class CreateInstitutions < ActiveRecord::Migration[5.0]
  def change
    create_table :institutions do |t|
      t.string :name
      t.string :acronym
      t.attachment :avatar
      t.string :officer_email
      t.string :officer_name
      t.integer :information_request_correlative
      t.timestamps
    end
  end
end
