class AddAreaFieldToUsersTable < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :administrative_area, :string, default: ''
  end
end
