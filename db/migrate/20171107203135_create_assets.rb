class CreateAssets < ActiveRecord::Migration[5.0]
  def change
    create_table :assets do |t|
      t.references :assetable, polymorphic: true
      t.attachment :attachment
      t.timestamps
    end
  end
end
