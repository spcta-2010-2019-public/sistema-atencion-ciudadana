class AddVisibleFieldToInstitutionsTable < ActiveRecord::Migration[5.0]
  def change
    add_column :institutions, :visible, :boolean, default: true
    add_index :institutions, :visible
  end
end
