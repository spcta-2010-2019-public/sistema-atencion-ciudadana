class AddUserFieldsToExpedientsTable < ActiveRecord::Migration[5.0]
  def change
    add_column :expedients, :age, :integer
    add_column :expedients, :gender, :string
    add_column :expedients, :state_id, :integer, index: true
    add_column :expedients, :city_id, :integer, index: true
  end
end
