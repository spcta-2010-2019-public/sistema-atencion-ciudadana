class AddAnonymousFieldToExpedientsTable < ActiveRecord::Migration[5.0]
  def change
    add_column :expedients, :anonymous, :boolean, default: false
  end
end
