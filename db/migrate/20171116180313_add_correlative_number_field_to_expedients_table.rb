class AddCorrelativeNumberFieldToExpedientsTable < ActiveRecord::Migration[5.0]
  def change
    add_column :expedients, :correlative_number, :integer, default: 0
  end
end
