class CreateManagementCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :management_categories do |t|
      t.string :classification, default: 's/c'
      t.references :institution, index: true
      t.string :kind, index: true
      t.string :name
      t.timestamps
    end
  end
end
