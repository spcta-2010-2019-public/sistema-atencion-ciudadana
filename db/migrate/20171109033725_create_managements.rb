class CreateManagements < ActiveRecord::Migration[5.0]
  def change
    create_table :managements do |t|
      t.references :expedient, index: true
      t.references :user, index: true
      t.string :kind
      t.references :management_category, index: true
      t.string :state, default: 'new'
      t.text :comment
      t.integer :assigned_ids, array: true, default: []
      t.integer :weight, default: 1
      t.string :closed_as
      t.timestamps
    end
  end
end
