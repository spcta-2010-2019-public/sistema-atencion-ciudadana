class AddClosureInfoToExpedientsTable < ActiveRecord::Migration[5.0]
  def change
    add_column :expedients, :resolution_type, :string, default: nil
    add_column :expedients, :it_proceeds, :integer, default: nil
  end
end
