module Arel
  module Predications
    def not_eq_or_null(other)
      left = not_eq(other)
      right = eq(nil)
      left.or(right)
    end
  end
end
