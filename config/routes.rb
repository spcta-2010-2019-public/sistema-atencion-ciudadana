Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  devise_for :users, controllers: { sessions: 'users/sessions' }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'home#index'

  resources :expedients, only: [:create] do
    get :check, on: :collection
    get :verify, on: :collection
    get :thanks, on: :member
  end

  resources :pages, only: [] do
    get :find, on: :collection
    get :thanks, on: :collection
  end

  get 'fati', to: 'home#new_whatsapp', as: :new_whatsapp
  post 'register_whatsapp', to: 'home#register_whatsapp', as: :register_whatsapp

  namespace :panel do
    root to: 'home#index'

    resources :profile, only: [:edit, :update]
    resources :versions, only: [:show]

    resources :admin_settings, only: [] do
      put :table_columns, on: :collection
    end

    resources :institution_calendars, only: [:index, :show, :new] do
      resources :calendar_events, only: [:new, :edit]
    end

    resources :expedients do
      put :updates, on: :collection
      put :destroys, on: :collection
      get :list, on: :collection
      get :documents, on: :member
      post :redirect, on: :member
      post :tags, on: :member
      resources :events, only: [:create, :update]
      resources :managements, only: [:create]
    end

    resources :managements, only: [:update] do
      post :close, on: :member
      resources :comments, only: [:create]
    end

    resources :statistics, only: [:index] do
      get :managementsassigned, on: :member
      get :administrativearea, on: :member
      get :scategories,        on: :member
      get :entryways,          on: :member
      get :branches,           on: :member
      get :details,            on: :collection
      get :cities,             on: :member
      get :tags,               on: :member
    end

    [
      :institutions,
      :oir_addresses,
      :calendar_events,
      :information_requests,
      :users,
      :occupations,
      :documents,
      :entry_ways,
      :management_categories,
      :branches
    ].each do |resource|
      resources resource do
        put :updates, on: :collection
        put :destroys, on: :collection
        get :list, on: :collection
      end
    end

    resources :institutions, only: [] do
      member do
        post 'settings'
        get 'unlock'
      end
    end
    resources :branches, only: [] do
      member do
        post 'settings'
        get 'unlock'
      end
    end
  end
end
