class CreateCalendarEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :calendar_events do |t|
      t.references :institution_calendar
      t.references :user
      t.integer :kind
      t.string :name
      t.text :description
      t.datetime :start_at
      t.datetime :end_at
      t.boolean :repetitive, default: false
      t.timestamps
    end
  end
end
