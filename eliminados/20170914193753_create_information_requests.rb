class CreateInformationRequests < ActiveRecord::Migration[5.0]
  def change
    create_table :information_requests do |t|
      t.references :institution
      t.integer :nationality
      t.string :nationality_name
      t.string :firstname
      t.string :lastname
      t.string :email
      t.integer :document_type
      t.string :document_number
      t.integer :entity_type
      t.attachment :document_front_image
      t.attachment :document_back_image
      t.attachment :other_document
      t.datetime :birthday
      t.integer :gender
      t.string :phone
      t.string :cellular
      t.string :fax
      t.integer :educational_level
      t.integer :residence
      t.text :address
      t.references :city
      t.references :occupation
      t.integer :delivery_way
      t.string :notification_mode
      t.string :reference_code
      t.boolean :confirmed
      t.datetime :confirmed_at
      t.date :admitted_at
      t.date :delivery_deadline
      t.integer :extension_type
      t.string :resolution_type
      t.string :correlative
      t.integer :redirected_institution_id
      t.boolean :seen_document
      t.boolean :ignore_it
      t.boolean :from_website
      t.string :resource
      t.string :state
      t.timestamps
    end
  end
end

#information_type: string
