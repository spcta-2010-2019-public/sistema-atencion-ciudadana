class CreateRequirements < ActiveRecord::Migration[5.0]
  def change
    create_table :requirements do |t|
      t.references :information_request
      t.references :user
      t.text :requested_information
      t.string :state
      t.attachment :file
      t.integer :priority
      t.string :extension_type
      t.string :resolution_type
      t.string :assigned_users
      t.string :invited_users
      t.timestamps
    end
  end
end
