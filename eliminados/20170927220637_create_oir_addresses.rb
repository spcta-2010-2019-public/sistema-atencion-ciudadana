class CreateOirAddresses < ActiveRecord::Migration[5.0]
  def change
    create_table :oir_addresses do |t|
      t.references :institution, foreign_key: true
      t.boolean :enabled, null: false, default: false
      t.decimal :lat, precision: 12, scale: 6, null: true, default: nil
      t.decimal :lng, precision: 12, scale: 6, null: true, default: nil
      t.string :phone, null: false, default: ''
      t.text :address
      t.text :schedule

      t.timestamps
    end
  end
end
