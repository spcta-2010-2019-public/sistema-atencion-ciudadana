# frozen_string_literal: true
#
namespace :expedient do

  desc 'Daily report expedients'
  task daily_report: :environment do
    ReportMailer.daily_report.deliver_now
  end

  desc 'Weekly report expedients'
  task weekly_report: :environment do
    ReportMailer.weekly_report.deliver_now
  end

end
